#!/usr/bin/env python

import wx
import layout
import os
from subprocess import Popen, PIPE
import _thread
import stat
import tempfile
import signal
import psutil
import multiprocessing

class FrameDefinitionExtension(layout.FrameDefinition):
    def __init__(self, *args, **kwds):
        self.launcherObject = launcher()
        if os.path.isfile(self.launcherObject.shortcutFilePath) == False:
            self.launcherObject.createDesktopShortcut()
        super().__init__(*args, **kwds)
        ardupilotBaseDir = ''
        while ardupilotBaseDir == '':
            ardupilotBaseDir = self.getArduPilotBase()
            if ardupilotBaseDir == "cancelled":
                self.showError('Cannot proceed without Ardupilot base directory. Exiting!')
                self.Destroy()
                return None
            if self.launcherObject.validateArdupilotDirecory(ardupilotBaseDir) == False:
                self.showError('Ardupilot Base Directory validation failed!')
                ardupilotBaseDir = ''
        self.launcherObject.ardupilotBaseDir = ardupilotBaseDir

    def getArduPilotBase(self):
        baseDirPickDialog = BaseDirectoryPickDialogExtension(self)
        if baseDirPickDialog.ShowModal() == wx.ID_OK:
            baseDir = baseDirPickDialog.GetPath()
            if os.path.isdir(baseDir):
                baseDirPickDialog.Destroy
                return baseDir
        else:
            baseDirPickDialog.Destroy()
            return "cancelled"
                
    def jsbSimToggleHandler(self, event):  # wxGlade: FrameDefinition.<event_handler>
        self.launcherObject.jsbSimToggle = self.JSBSimToggleButton.GetValue()
        if self.JSBSimToggleButton.GetValue() == True:
            aircraftPickDialog = AircraftScriptFilePickDialogExtension(self)
            aircraftPickDialog.SetDirectory(self.launcherObject.getAircraftScriptDir())
            if aircraftPickDialog.ShowModal() == wx.ID_OK:
                filePath = aircraftPickDialog.GetPath()
                if os.path.isfile(filePath):
                    fileName = os.path.basename(filePath)
                    self.launcherObject.aircraftMissionScriptFile = fileName
                    aircraftPickDialog.Destroy
            else:
                aircraftPickDialog.Destroy()
                self.JSBSimToggleButton.SetValue(False)
                self.Refresh()

    def consoleToggleHandler(self, event):
        self.launcherObject.consoleToggle = self.ConsoleToggleButton.GetValue()

    def mapToggleHandle(self, event):
        self.launcherObject.mapToggle = self.MapToggleButton.GetValue()

    def debugToggleHandler(self, event):
        self.launcherObject.debugToggle = self.DebugToggleButton.GetValue()

    def startStopSimHandler(self, event):
        if self.StartStopSim.GetValue() == True:
            self.subProcessInstance = self.launcherObject.launchProgram()
            self.StartStopSim.SetLabel("Stop Simulation")
        elif self.StartStopSim.GetValue() == False:
            if self.subProcessInstance.pid is None:
                Terminator().terminate(None)
            else:
                Terminator().terminate(self.subProcessInstance.pid)
            self.StartStopSim.SetLabel("Start Simulation")
                
    def rebaseButtonHandler(self, event):
        ardupilotBaseDir = self.getArduPilotBase()
        if self.launcherObject.validateArdupilotDirecory(ardupilotBaseDir) == False:
            self.showError('Ardupilot Base Directory validation failed!')
        else:
            self.launcherObject.ardupilotBaseDir = ardupilotBaseDir

    def showError(self, message):
        wx.MessageBox(message, 'Warning', wx.OK | wx.ICON_WARNING)


class AircraftScriptFilePickDialogExtension(layout.AircraftScriptFilePickDialog):
    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)
        self.SetWildcard("XML Files (*.xml)|*.xml|(*.XML)|*.XML")

class BaseDirectoryPickDialogExtension(layout.BaseDirectoryPickDialog):
    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)

class launcher:
    def __init__(self):
        self.shortcutFilePath = "/usr/share/applications/rPilot-sitl-launcher.desktop"
        self.applicationName = "rPilot\ SITL\ Helper"
        self.programName = 'sim_vehicle.py'
        self.aircraftTypeArg = '-v'
        self.aircraftVariant = 'ArduPlane'
        self.ardupilotAricraftModelArg = '-f'
        self.ardupilotJSBSimPrefix = 'jsbsim:'
        self.ardupilotConsoleArg = '--console'
        self.ardupilotMapArg = '--map'
        self.ardupilotDebugArg = '-D'
        self.ardupilotBaseDir = ''
        self.aircraftScriptDir = '/Tools/autotest/JSBSimMissionScripts/'
        self.ardupilotArduPlaneDirectoryName = '/ArduPlane'
        self.multiprocessorFlag = '-j' + str(multiprocessing.cpu_count())

        self.jsbSimToggle = False
        self.consoleToggle = False
        self.mapToggle = False
        self.debugToggle = False
        self.subProcessInstance = None
    
    def validateArdupilotDirecory(self, baseDir):
        targetFile = baseDir + '/Tools/autotest/sim_vehicle.py'
        if os.path.isfile(targetFile):
            return True
        return False

    def getAircraftScriptDir(self):
        return self.ardupilotBaseDir + self.aircraftScriptDir

    def setJSBSimToggle(self, toggleValue):
        self.jsbSimToggle = toggleValue

    def setAircraftMissionScriptFile(self, aircraftMissionScriptFile):
        self.aircraftMissionScriptFile = aircraftMissionScriptFile

    def setConsoleToggle(self, toggleValue):
        self.consoleToggle = toggleValue

    def setMapToggle(self, toggleValue):
        self.mapToggle = toggleValue

    def setDebugToggle(self, toggleValue):
        self.debugToggle = toggleValue

    def createDesktopShortcut(self):
        fileContent = "[Desktop Entry]\nType=Application\nTerminal=false\nName=rPilot SITL Helper"
        currentDir = os.path.abspath(os.path.curdir)
        fileContent += "\nIcon="+currentDir+"/rflight-icon.png"
        fileContent += "\nExec="+currentDir+"/"+self.applicationName
        try:
            if os.path.isfile(self.shortcutFilePath) == False:
                desktopShortcut = open(self.shortcutFilePath, "w")
                desktopShortcut.write(fileContent)
                desktopShortcut.close()
        except Exception as e:
            print(e)


    def launchProgram(self):
        Terminator().terminate(None)
        space = ' '
        commandString = 'cd' + space + self.ardupilotBaseDir + self.ardupilotArduPlaneDirectoryName
        commandString += '\n' + self.programName
            
        if self.jsbSimToggle == True:
            commandString += space + self.ardupilotAricraftModelArg + space + self.ardupilotJSBSimPrefix + self.aircraftMissionScriptFile
        
        if self.consoleToggle == True:
            commandString += space + self.ardupilotConsoleArg
            #argList.append(self.ardupilotConsoleArg)

        if self.mapToggle == True:
            commandString += space + self.ardupilotMapArg
            #argList.append(self.ardupilotMapArg)

        if self.debugToggle == True:
            commandString += space + self.ardupilotDebugArg
            #argList.append(self.ardupilotDebugArg)
        
        commandString += space + self.multiprocessorFlag

        commandSh = open("/tmp/command", "w")
        commandSh.write("#!/usr/bin/env bash\n" + commandString)
        commandSh.close()
        st = os.stat(commandSh.name)
        os.chmod(path=commandSh.name, mode=st.st_mode | stat.S_IEXEC)
        argList = ['gnome-terminal' , '-x', 'bash', '-c', os.path.abspath(commandSh.name)]
        subProcessInstance = Popen(argList, stdin = PIPE, stdout = PIPE, stderr = PIPE, bufsize = 1, universal_newlines = True)
        return subProcessInstance

class Terminator:
    def __init__(self):
        self.killAllCommand = 'killall'
        self.targetAliases = ['arduplane', 'mavproxy.py', 'JSBSim'] 

    def terminate(self, pidToKill):
        targets = findProcessIdByName(self.targetAliases)
        for target in targets:
            os.kill(target, signal.SIGKILL)
        if pidToKill is not None:
            os.kill(pidToKill, signal.SIGKILL)

class Main(wx.App):
    def OnInit(self):
        self.frame = FrameDefinitionExtension(None, wx.ID_ANY, "")
        if self.frame is None:
            return False
        self.SetTopWindow(self.frame)
        self.frame.Show()
        return True

# end of class Main

def findProc(processName):
    dirList = os.listdir("/proc")
    bufsize = 1024
    if dirList != None:
        while dir in dirList:
            try:
                pid = int(dir.name)
                if pid is not None:
                    cmdFile = open("/proc/" + pid + "/cmdline", "r")
                    if cmdFile is not None:     
                        cmdSplit = cmdFile.read(bufsize).split(sep="--")
                        cmd = cmdSplit.pop(0)
                        if cmd.__contains__(processName):
                            return pid
            except:
                continue
    return None

def findProcessIdByName(processNameList):
   procNameToIdDict = {}
   for proc in psutil.process_iter():
      try:
         pinfo = proc.as_dict(attrs=['pid', 'name', 'create_time'])
         print()
         procNameToIdDict.setdefault(pinfo['name'], []).append(int(pinfo['pid']))
      except (psutil.NoSuchProcess, psutil.AccessDenied , psutil.ZombieProcess):
         pass
   listOfPids = []
   for proc in processNameList:
      if procNameToIdDict.get(proc) is not None:
         listOfPids.extend(procNameToIdDict.get(proc))
   return listOfPids


if __name__ == "__main__":
    rFlightSITLHelper = Main(None)
    if rFlightSITLHelper == False:
        exit()
    rFlightSITLHelper.MainLoop()
